# Docker container for buiding Petalinux (2022.2) projects

This container is intended to be used in CI and Windows systems to build Petalinux image for the FELIX project.

## Requirements

To use this container, you need a Docker installation ([https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)).

## Download a pre-build container

Pull the container:
```
docker pull gitlab-registry.cern.ch/atlas-tdaq-felix/felix-versal-tools/petalinux-docker-ci
```

## Build container from scratch

You can also build the container from scratch, e.g if you want to use a different petalinux version.

Clone this repository and change into the repository directory:
```
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-versal-tools/petalinux-docker-ci.git
cd petalinux-docker-ci
```

Download an [installer for Petalinux](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools/2022-2.html). This image has been tested with Petalinux 2022.2. Save the Petalinux installer (\*.run file) into the root folder repository. 

Build the container:
```
docker build -t gitlab-registry.cern.ch/atlas-tdaq-felix/felix-versal-tools/petalinux-docker-ci .
```

## Run the container, build petalinux image
You can start the container and build your image running these commands:
```
docker run -it --rm -v <path_to_petalinux_project_files>:/home/petalinux/build gitlab-registry.cern.ch/atlas-tdaq-felix/felix-versal-tools/petalinux-docker-ci
petalinux-build
petalinux-package --boot --u-boot --force
```

## Publish the container

If you are not yet logged into the container registry: 
```
docker login gitlab-registry.cern.ch 
```

Push the container:
```
docker push gitlab-registry.cern.ch/atlas-tdaq-felix/felix-versal-tools/petalinux-docker-ci
```